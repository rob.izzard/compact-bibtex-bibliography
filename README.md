# Compact Bibliography

Are you fed up with a long, unwieldy bibliography when using LaTeX, BiBTeX and Natbib? Don't want to hack the TeX yourself or make it by hand? Then try this.

1) Install `robcompact2023.bst` in one of your LaTeX directories, e.g. `$HOME/.latex`.
2) Use `robcompact2023` as your bibliography style file.
3) Insert the contents of `preamble.tex` in your document's preamble.

Done! 

## Preamble

In your preamble you may wish to add the astronomy/astrophysics macro set
`aas_macros` and also set the font to use Sans Serif rather than the default Roman

```
% use aas (astronomy/astrophysics) macros and use sans style
\usepackage{aas_macros}
\let\jnl@style=\sf
```

## Inserting the bibliography

I recommend inserting the bibliography with this LaTeX code which removes the "References" section title and shifts the references up a bit. You should experiment with the `-5mm` to match your document, and of course set your `bibfilename` appropriately.

```
% use Rob's compact bibliography
\renewcommand{\refname}{}
\vspace{-5mm}
\bibliographystyle{robcompact2023}
\bibliography{bibfilename}
```

## Results

The references list should then look like this:

![](compact_reflist.png)


Any problems, just let me know.

https://gitlab.com/rob.izzard/compact-bibtex-bibliography

## Author
Robert Izzard, 2023.

The style file `robcompact2023.bst` was originally made with `makebst`.
The content of `preamble.tex` is based on natbib https://ctan.org/pkg/natbib?lang=en

## License
LaTeX https://www.latex-project.org/lppl/ 
